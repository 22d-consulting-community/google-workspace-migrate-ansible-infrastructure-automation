#--- Downlaod and export the Platform Server---
# URL of the zip file to download
$url = "https://dl.google.com/google-workspace-migrate/ga/GoogleWorkspaceMigrate_Installers.zip"

#Local path to save the zip file
$localPath = "C:\Users\admin\GoogleWorkspaceMigrate_Installers.zip"
#Location to extract the contents of the zip file
$extractPath = "C:\Users\admin\GWM"
#Download the zip file
Invoke-WebRequest -Uri $url -OutFile $localPath
#Extract the contents of the zip file
Expand-Archive -Path $localPath -DestinationPath $extractPath

#---Install the Platform Server---
$PROJECT_ID="gwm-adel-test-proj"
$clientid = "564852873485-n6m07ctcnl6an3eh5h8sicr8lnufip4m.apps.googleusercontent.com"
$serviceaccountkeypath = "C:\Users\admin\gwm-sa-$PROJECT_ID-key.json"
$adminemail = "ahmed@demo-ent.22d.tech"
#Check the version number and extractPath
C:\Users\admin\GWM\GoogleWorkspaceMigrate_Platform_Installer_2.4.0.0.exe /CLIENTID=$clientid /SERVICEACCOUNT=$serviceaccountkeypath /ADMINEMAIL=$adminemail /PORT=5131 /SP- /VERYSILENT /SUPPRESSMSGBOXES /NORESTART
Start-Service -Name AppBridge.Local.ServiceHost
