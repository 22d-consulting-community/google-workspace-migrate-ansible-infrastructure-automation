#--- Downlaod and export the Node Server---
# URL of the zip file to download
$url = "https://dl.google.com/google-workspace-migrate/ga/GoogleWorkspaceMigrate_Installers.zip"

#Local path to save the zip file
$localPath = "C:\Users\admin\GoogleWorkspaceMigrate_Installers.zip"
#Location to extract the contents of the zip file
$extractPath = "C:\Users\admin\GWM"
#Download the zip file
Invoke-WebRequest -Uri $url -OutFile $localPath
#Extract the contents of the zip file
Expand-Archive -Path $localPath -DestinationPath $extractPath

#---Install the Node Server---
$accesskey = "q8Kl8Tq1MOI"
#Check the version number and extractPath
C:\Users\admin\GWM\GoogleWorkspaceMigrate_Node_Installer_2.4.0.0.exe /ACCESSKEY=$accesskey /NORESTART /SP- /SUPPRESSMSGBOXES /VERYSILENT
Start-Service -Name AppBridge.Local.ServiceNode
